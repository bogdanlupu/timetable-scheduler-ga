import json
import time

from debug import peek_population
from fitness import fitness
from ga_operators import crossover_population, mutate_population
from input.utils import get_input, print_schedule
from population_generator import generate_population
from ga_selectors import elitist_selector
import matplotlib.pyplot as plt
import numpy as np
import streamlit as st


class GA(object):
    def __init__(self, population_size=100,
                 iterations=None,
                 mutation_percentage=0.5,
                 mutation_flavor_percentage=0.3,
                 crossover_percentage=0.5,
                 enable_logging=True,
                 ui_elements=None) -> None:
        self.population_size = population_size
        self.iterations = iterations
        self.mutation_percentage = mutation_percentage
        self.mutation_flavor_percentage = mutation_flavor_percentage
        self.crossover_percentage = crossover_percentage
        self.max_fitness = 35
        self.enable_logging = enable_logging
        self.ui_elements = ui_elements

    def update_ui(self, data):
        fitness_list: list = data['fitness']
        max_fitness = fitness_list[-1]
        self.ui_elements['status text'].text(f'Max fitness {max_fitness}')

        # self.ui_elements['fitness chart'].add_rows(fitness_list[len(hist_fitness_list):])
        chart_range = np.arange(0, data['iteration'])
        plt.plot(chart_range, data['fitness'], c='m')
        plt.plot(chart_range, data['uniqueness'], c='r')
        self.ui_elements['fitness chart'].pyplot(plt)

        data['hist fitness'] = fitness_list.copy()

    def genetic_algorithm(self, classes):
        start_time = time.time()

        population = generate_population(classes, self.population_size)
        data = {
            'fitness': [],
            'hist fitness': [],
            'uniqueness': [],
            'iteration': 0
        }
        best_fitness = 0
        best_individual = ''
        it = 0

        while best_fitness < self.max_fitness:
            it += 1
            data['iteration'] = it
            if self.iterations is not None and it >= self.iterations:
                print(f'Maximum iterations reached. Best fitness {best_fitness}')
                break

            # if it % 10 == 0:
            #     print(f'Iteration {it}')
            # population = selector(population)
            population = elitist_selector(population)
            mutate_population(population, self.mutation_percentage, self.mutation_flavor_percentage)
            crossover_population(population, self.crossover_percentage)
            # best_individual = max(population, key=lambda candidate: fitness(candidate))

            new_maximum = max([fitness(candidate) for candidate in population])
            data['fitness'].append(new_maximum / self.max_fitness)
            if new_maximum > best_fitness:
                print(f'New maximum obtained ({new_maximum}) in iteration {it}')
                best_fitness = new_maximum

            best_current_individual, population_dict = peek_population(
                population,
                enable_logging=self.enable_logging)
            best_individual_str = json.dumps(best_current_individual, sort_keys=True)
            data['uniqueness'].append(len(population_dict) / 100)
            if best_individual_str != best_individual:
                best_individual = best_individual_str
                # print(f'Best individual [{fitness(best_individual)}]: \t{best_individual}')

            if it % 10 == 0:
                self.update_ui(data=data)

        duration = time.time() - start_time
        with open('results.txt', 'a') as f:
            f.write(
                f'{max([fitness(candidate) for candidate in population])} {max(population, key=lambda candidate: fitness(candidate))}\n')

        it_range = np.arange(0, it)
        plt.plot(it_range, data['fitness'], c='m')
        plt.plot(it_range, data['uniqueness'], c='r')

        st.pyplot(plt)

        return {
            "duration": duration,
            "best_candidate": max(population, key=lambda candidate: fitness(candidate))
        }


if __name__ == '__main__':
    st.title('Timetable with genetic algorithms')
    progress_bar = st.sidebar.progress(0)
    status_text = st.sidebar.empty()
    last_rows = np.random.randn(1, 1)
    fig, axes = plt.subplots()
    axes.plot([], [], c='m')
    axes.plot([], [], c='r')

    chart = st.pyplot(plt)

    ui_elements = {
        'progress bar': progress_bar,
        'status text': status_text,
        'fitness chart': chart,
        'fitness chart axes': axes
    }

    input_data = get_input()
    all_classes = input_data["assignments"]
    ga = GA(enable_logging=False,
            ui_elements=ui_elements)

    result = ga.genetic_algorithm(all_classes)
    selected_individual = result["best_candidate"]
    duration = result["duration"]
    print(f'Time: {str(duration)}')
    st.write('Duration: ', str(duration))

    for class_index in range(len(selected_individual)):
        print_schedule(
            title=input_data["classes"][class_index],
            teachers=input_data["teachers"],
            schedule=selected_individual[class_index]
        )
